#include <iostream>
#include <math.h>
#include <time.h>

using namespace std;

//poniższa zmienna tworzy nam tablicę dwuwymiarową
//która będzie miała 10 pozycji (odpowiednik 10 wierszy w Excel)
//i będzie zawierać 2 pozycje w każdym z tych wierszy (odpowiednik kolumn w Excel)
//float zmianaKursu[10][2];

#define WIERSZE 2
#define KOLUMNY 2
//pokazać const oraz enum

float zmianaKursu[WIERSZE][KOLUMNY];

void liczKwadrat(int bok);
void dodajKurs(float pln, float usd, int pos);

void dodajKurs2(float waluty[],int pos);

void obwWielokat()
{
    unsigned int katy=1;
    int suma =0;
    cout << "Witaj! Podaj ilosc katow figury:" << endl;
    cin >> katy;
    //odcinki[0] odcinki[1] odcinki[2] odcinki[3]
//    int odcinki[4];
//    odcinki[0] = 4;
//    odcinki[1] = 2;
//    odcinki[2] = 7;
//    odcinki[3] = 1;
//    int odcinki[] = {4,2,7,1};
//    cout << "Obwod naszego wielokata: " << odcinki[0]+odcinki[1]+odcinki[2]+odcinki[3] << endl;
    //tablica jednowymiarowa
    int odcinki[katy];
    for(int i=0;i<katy;i++)
    {
        cout << "Podaj dlugosc odcinka numer " << i+1 << ": ";
        cin >> odcinki[i];
    }
//    for(int i=0;i<katy;i++)
//        suma+=odcinki[i];
//    cout << "Obwod naszego wielokata: " << suma << endl;
    for(int i=1;i<katy;i++)
        odcinki[0]+=odcinki[i];
    cout << "Obwod naszego wielokata: " << odcinki[0] << endl;
    cout << "Podane odcinki to: " << endl;
    for (int i=katy/2;i<katy;i++)
    {
        cout << "Odcinek " << i+1;
        if (i ==0)
        {
            for (int j=1; j<katy;j++)
                odcinki[i]-=odcinki[j];
        }
        cout << " ma wartosc " << odcinki[i] << endl;
        if(i==katy-1) i =-1;
        if(i==katy/2-1) break;
    }
}

int poleObwod()
{
    int a=0,b=0,c=0,d=0,o,p,menu;
    cout << "Witaj! Wybierz jedna z opcji:" << endl
        << "1 - kwadrat" << endl
        << "2 - prostokat" << endl
        << "3 - trojkat" << endl
        << "4 - romb" << endl
        << "5 - zabawa z losowaniem" << endl
        << "6 - zakonczenie" << endl;
    cin >> menu;
    if (menu==5) return 1;
    if (menu == 6) exit(0);
    cout << "Podaj wartosci odcinkow dla figury: ";
    switch(menu) {
        case 1: cin >> a; liczKwadrat(a); break;
        case 2: cin >>a >> b; break;
        case 3: cin >>a>>b>>c>>d; break;
        case 4: cin >>a>>b; break;
    }
    cout << a << ' ' << b << ' ' << c << ' ' << d << endl;
    return 0;
}

int main()
{
    srand(time(NULL));
    int a, b;

    for(;;)
    {
        a = 0;
//        for (int i=0;i<WIERSZE;i++) {
//            float c[KOLUMNY];
//            for (int j=0;j<KOLUMNY;j++)
//            {
//                cout << "Podaj wartosc waluty " << j+1 << ": ";
//                cin >> c[j];
//            }
//            dodajKurs2(c,i);
//            //float c,b;
//            //cin >>c>>b;
//            //dodajKurs(c,b,i);

//        }
//        for (int i=0;i<WIERSZE;i++) {
//            cout << "Zmiana kursu spod wiersza " << i+1;
//            for (int j=0;j<KOLUMNY;j++)
//                cout << "Wartosc waluty " << j+1 << ": " << zmianaKursu[i][j] << endl;
//            cout << endl;
//        }
        //obwWielokat(); <- uruchomić i zrobić obsługę błędów oraz dać możliwość przerwania działania funkcji jak w poleObwod()
        //a = poleObwod();
        if (a == 1) break;
    }

    while(1)
    {
        a = rand()%100;
        cout << "zgadnij wylosowana liczbe: ";
        cin >> b;
        if (b < 0) break;
        if (a == b)
            cout << "Udalo Ci sie! To liczba " << b << endl;
        else
            cout << "liczba wysolowana " << a << " Twoja to " << b << endl;
    }

    cout << "Koncze program";

    return 0;
}

void liczKwadrat(int bok)
{
    int o=4*bok;
    int p=bok*bok;
    cout << "Pole kwadratu wynosi: " << p << " oraz obwod: " << o << endl;
}

void dodajKurs(float pln, float usd, int pos) {
    zmianaKursu[pos][0] = pln;
    zmianaKursu[pos][1] = usd;
}

void dodajKurs2(float waluty[], int pos)
{
    //następnym razem wskazać możliwości static
    for (int i=0; i < KOLUMNY;i++)
        zmianaKursu[pos][i] = waluty[i];
}
